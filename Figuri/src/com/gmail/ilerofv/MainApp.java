package com.gmail.ilerofv;

import com.gmail.ilerofv.Figures.CircleP;
import com.gmail.ilerofv.Figures.Figure;
import com.gmail.ilerofv.Figures.Paper;
import com.gmail.ilerofv.Figures.RectangleP;
import com.gmail.ilerofv.Figures.TriangleP;

public class MainApp
{
    public static void main(String[] args)
    {
        Paper fig1 = new RectangleP(2,3);
        Paper fig2 = new CircleP();
        Paper fig3 = new TriangleP();
        Paper fig4 = new RectangleP(fig1);
        System.out.println(fig1);
        System.out.println(fig2);
        System.out.println(fig3);
        System.out.println(fig4);
    }
}
