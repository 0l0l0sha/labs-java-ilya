package com.gmail.ilerofv.Figures;

import java.util.Objects;

public class RectangleP extends Rectangle implements Paper
{
    private ColorHolder colorHolder = new ColorHolder();

    public RectangleP()
    {
        super();

    }

    public RectangleP(Paper figure)
    {
        super(figure.proportion(),figure.proportion());
        if(figure.isColored())
        {
            this.setColor(figure.getColor());
        }
    }

    public RectangleP(double high, double width)
    {
        super(high, width);
    }

    public RectangleP(double high, double width, String color)
    {
        super(high,width);
        this.setColor(color);
    }

    @Override
    public boolean isColored()
    {
        return colorHolder.isColored();
    }

    @Override
    public boolean setColor(String color)
    {
        return colorHolder.setColor(color);
    }

    @Override
    public String getColor()
    {
        return colorHolder.getColor().toString();
    }

    @Override
    public double proportion()
    {
        return getHigh() < getWidth() ? getHigh() : getWidth();
    }



    @Override
    public String toString()
    {
        return "Paper Rectangle{" +
                "high=" + getHigh() +
                ", width=" + getWidth() +
                ", color='" + getColor() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        RectangleP rectangle = (RectangleP) o;
        return getColor().equals(rectangle.getColor());
    }


    @Override
    public int hashCode()
    {
        return Objects.hash(getHigh(), getWidth(), getColor());
    }
}
