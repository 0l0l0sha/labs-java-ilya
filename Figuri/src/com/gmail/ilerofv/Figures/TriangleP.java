package com.gmail.ilerofv.Figures;

import java.util.Objects;

import static java.lang.Math.sqrt;

public class TriangleP extends Triangle implements Paper
{
    private ColorHolder colorHolder = new ColorHolder();

    public TriangleP(double edge)
    {
        super(edge);
    }

    public TriangleP()
    {
        super();
    }

    public TriangleP(double edge, String color)
    {
        super(edge);
        this.setColor(color);
    }

    public TriangleP(Paper figure)
    {
        super(figure.proportion());
        if (figure.isColored())
        {
            this.setColor(figure.getColor());
        }
    }

    @Override
    public double proportion()
    {
        return (getEdge() / 4) * sqrt(3);
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        TriangleP triangle = (TriangleP) o;
        return getColor().equals(triangle.getColor());
    }

    @Override
    public String toString()
    {
        return "Paper Triangle{" +
                "edge=" + getEdge() +
                ", color='" + getColor() + '\'' +
                '}';
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getEdge(), getColor());
    }

    @Override
    public boolean isColored()
    {
        return colorHolder.isColored();
    }

    @Override
    public boolean setColor(String color)
    {
        return colorHolder.setColor(color);
    }

    @Override
    public String getColor()
    {
        return colorHolder.getColor().toString();
    }
}
