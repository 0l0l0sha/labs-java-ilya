package com.gmail.ilerofv.Figures;

public interface Membrane
{
    String getColor();
    double proportion();
}
