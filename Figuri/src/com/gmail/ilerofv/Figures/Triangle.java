package com.gmail.ilerofv.Figures;

import java.util.Objects;

import static java.lang.Math.sqrt;

public abstract class Triangle extends Figure
{
    private double edge;

    public Triangle(double edge)
    {
        super();
        this.edge = edge;
    }

    public Triangle()
    {
        super();
        this.edge = 2 / sqrt(5);
    }


    @Override
    public double perimeter()
    {
        return edge * 3;
    }

    @Override
    public double square()
    {
        return edge * edge * sqrt(5) / 2;
    }
    public double getEdge()
    {
        return edge;
    }

    public void setEdge(double edge)
    {
        this.edge = edge;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.edge, edge) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(edge);
    }
}
