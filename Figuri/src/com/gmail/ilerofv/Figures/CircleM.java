package com.gmail.ilerofv.Figures;

import java.util.Objects;

import static java.lang.Math.PI;

public class CircleM extends Circle implements Membrane
{

    public CircleM(double r)
    {
        super(r);

    }

    public CircleM()
    {
        super();
    }

    public CircleM(Membrane figure)
    {
        super(figure.proportion() / 2);
    }

    @Override
    public double proportion()
    {
        return getRadius();
    }



    @Override
    public String toString()
    {
        return "Membrane Circle{" +
                "radius=" + getRadius() +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        return super.equals(o);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getRadius());
    }

    @Override
    public String getColor()
    {
        return "None";
    }


}
