package com.gmail.ilerofv.Figures;

import java.util.Objects;

public class Rectangle extends Figure
{

    private double high, width;

    public Rectangle()
    {
        super();
        this.high = 1;
        this.width = 1;

    }

    public Rectangle(double high, double width)
    {
        super();
        this.high = high;
        this.width = width;
    }
    @Override
    public double perimeter()
    {
        return high + width;
    }

    @Override
    public double square()
    {
        return high * width;
    }

    public double getHigh()
    {
        return high;
    }

    public void setHigh(double high)
    {
        this.high = high;
    }

    public double getWidth()
    {
        return width;
    }

    public void setWidth(double width)
    {
        this.width = width;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.high, high) == 0 &&
                Double.compare(rectangle.width, width) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(high, width);
    }
}
