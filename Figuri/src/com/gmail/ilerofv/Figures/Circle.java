package com.gmail.ilerofv.Figures;

import java.util.Objects;

import static java.lang.Math.PI;

public abstract class Circle extends Figure
{
    private double radius;
    public Circle(double radius)
    {
        super();
        this.radius = radius;
    }

    public Circle()
    {
        super();
        this.radius = 1;
    }

    @Override
    public double perimeter()
    {
        return radius * PI * 2;
    }

    @Override
    public double square()
    {
        return radius * radius * PI;
    }

    public double getRadius()
    {
        return radius;
    }

    public void setRadius(double radius)
    {
        this.radius = radius;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0;
    }

}
