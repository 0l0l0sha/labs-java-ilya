package com.gmail.ilerofv.Figures;

import java.util.Objects;

public abstract class Figure
{

    public Figure()
    {
    }





    public abstract double perimeter();

    public abstract double square();

    public boolean equals(Object o)
    {
        if (o == null || getClass() != o.getClass()) return false;
        return true;
    }
}
