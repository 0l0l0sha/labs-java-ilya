package com.gmail.ilerofv.Figures;

import java.util.Objects;

import static java.lang.Math.sqrt;

public class TriangleM extends Triangle implements Membrane
{

    public TriangleM(double edge)
    {
        super(edge);
    }

    public TriangleM()
    {
        super();
    }

    public TriangleM(Membrane figure)
    {
        super(figure.proportion());
    }

    @Override
    public double proportion()
    {
        return (getEdge() / 4) * sqrt(3);
    }

    @Override
    public boolean equals(Object o)
    {
        return super.equals(o);
    }

    @Override
    public String toString()
    {
        return "Membrane Triangle{" +
                "edge=" + getEdge() +
                '}';
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getEdge());
    }



    @Override
    public String getColor()
    {
        return "None";
    }

}
