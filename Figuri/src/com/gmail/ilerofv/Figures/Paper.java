package com.gmail.ilerofv.Figures;

public interface Paper
{
    boolean isColored();

    boolean setColor(String color);

    String getColor();

    double proportion();

    class ColorHolder
    {
        private Color color;
        private boolean colored = false;

        public boolean isColored()
        {
            return colored;
        }

        public boolean setColor(Color color)
        {
            if (colored) return false;
            this.color = color;
            colored = true;
            return true;
        }

        boolean setColor(String color)
        {
           return setColor(Color.valueOf(color));
        }

        public Color getColor()
        {
            if(!isColored())
            {
                return Color.NePokraseno;
            }
            return color;
        }

    }
}