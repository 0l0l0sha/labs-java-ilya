package com.gmail.ilerofv.Figures;

import java.util.Objects;

import static java.lang.Math.PI;

public class CircleP extends Circle implements Paper
{
    private ColorHolder colorHolder = new ColorHolder();

    public CircleP(double radius, String color)
    {
        super(radius);
        this.setColor(color);
    }

    public CircleP(double r)
    {
        super(r);
    }

    public CircleP()
    {
        super();
    }

    public CircleP(Paper figure)
    {
        super(figure.proportion() / 2);
        if (figure.isColored())
        {
            this.setColor(figure.getColor());
        }
    }

    @Override
    public double proportion()
    {
        return this.getRadius();
    }

    @Override
    public String toString()
    {
        return "Paper Circle{" +
                "radius=" + this.getRadius() +
                ", color='" + getColor() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        CircleP circle = (CircleP) o;
        return getColor().equals(circle.getColor());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getRadius(), getColor());
    }

    @Override
    public boolean isColored()
    {
        return colorHolder.isColored();
    }

    @Override
    public boolean setColor(String color)
    {
        return colorHolder.setColor(color);
    }

    @Override
    public String getColor()
    {
        return colorHolder.getColor().toString();
    }
}
