package com.gmail.ilerofv.Figures;

import java.util.Objects;

public class RectangleM extends Rectangle implements Membrane
{

    public RectangleM()
    {
        super();
    }

    public RectangleM(Membrane figure)
    {
        super(figure.proportion(),figure.proportion());
    }

    public RectangleM(double high, double width)
    {
        super(high,width);
    }


    @Override
    public String getColor()
    {
        return "None";
    }

    @Override
    public double proportion()
    {
        return getHigh() < getWidth() ? getHigh() : getWidth();
    }


    @Override
    public String toString()
    {
        return "Membrane Rectangle{" +
                "high=" + getHigh() +
                ", width=" + getWidth()+
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        return super.equals(o);
    }


    @Override
    public int hashCode()
    {
        return Objects.hash(getHigh(), getWidth());
    }
}
